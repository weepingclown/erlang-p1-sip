erlang-p1-sip (1.0.49-1) unstable; urgency=medium

  * New upstream version 1.0.49
  * Refreshed patches
  * Updated Erlang dependencies
  * Updated Standards-Version: 4.6.2 (no changes needed)
  * Updated years in debian/copyright

 -- Philipp Huebner <debalance@debian.org>  Wed, 08 Feb 2023 14:15:33 +0100

erlang-p1-sip (1.0.48-1) unstable; urgency=medium

  * New upstream version 1.0.48
  * Updated Erlang dependencies
  * Refreshed debian/patches/rebar.config.diff

 -- Philipp Huebner <debalance@debian.org>  Wed, 02 Nov 2022 14:57:35 +0100

erlang-p1-sip (1.0.47-1) unstable; urgency=medium

  * New upstream version 1.0.47
  * Updated Standards-Version: 4.6.1 (no changes needed)
  * Updated Erlang dependencies
  * Updated years in debian/copyright

 -- Philipp Huebner <debalance@debian.org>  Thu, 02 Jun 2022 12:36:55 +0200

erlang-p1-sip (1.0.45-1) unstable; urgency=medium

  [ Philipp Huebner ]
  * New upstream version 1.0.45
  * Updated Erlang dependencies
  * Updated debian/watch

  [ Jenkins ]
  * Remove constraints unnecessary since buster

 -- Philipp Huebner <debalance@debian.org>  Mon, 20 Dec 2021 00:15:18 +0100

erlang-p1-sip (1.0.43-1) unstable; urgency=medium

  * New upstream version 1.0.43
  * Updated Standards-Version: 4.6.0 (no changes needed)
  * Updated Erlang dependencies

 -- Philipp Huebner <debalance@debian.org>  Sun, 29 Aug 2021 21:54:13 +0200

erlang-p1-sip (1.0.41-2) unstable; urgency=medium

  * Corrected Multi-Arch setting to "allowed"

 -- Philipp Huebner <debalance@debian.org>  Sun, 31 Jan 2021 19:20:05 +0100

erlang-p1-sip (1.0.41-1) unstable; urgency=medium

  * New upstream version 1.0.41
  * Added 'Multi-Arch: same' in debian/control
  * Updated Erlang dependencies
  * Updated years in debian/copyright

 -- Philipp Huebner <debalance@debian.org>  Sat, 30 Jan 2021 18:30:43 +0100

erlang-p1-sip (1.0.39-1) unstable; urgency=medium

  * New upstream version 1.0.39
  * Updated Standards-Version: 4.5.1 (no changes needed)
  * Updated Erlang dependencies
  * Updated version of debian/watch: 4
  * Updated debhelper compat version: 13
  * Updated lintian overrides
  * Added debian/erlang-p1-sip.install
  * Refreshed debian/patches/fix-includes.diff

 -- Philipp Huebner <debalance@debian.org>  Sat, 26 Dec 2020 00:24:28 +0100

erlang-p1-sip (1.0.37-1) unstable; urgency=medium

  * New upstream version 1.0.37
  * Updated Erlang dependencies

 -- Philipp Huebner <debalance@debian.org>  Sun, 02 Aug 2020 17:39:54 +0200

erlang-p1-sip (1.0.35-1) unstable; urgency=medium

  * New upstream version 1.0.35
  * Updated Erlang dependencies

 -- Philipp Huebner <debalance@debian.org>  Sun, 26 Jul 2020 22:17:16 +0200

erlang-p1-sip (1.0.33-1) unstable; urgency=medium

  * New upstream version 1.0.33
  * Updated Erlang dependencies

 -- Philipp Huebner <debalance@debian.org>  Fri, 01 May 2020 13:36:44 +0200

erlang-p1-sip (1.0.32-1) unstable; urgency=medium

  * New upstream version 1.0.32
  * Updated Erlang dependencies

 -- Philipp Huebner <debalance@debian.org>  Wed, 18 Mar 2020 12:33:37 +0100

erlang-p1-sip (1.0.31-1) unstable; urgency=medium

  * New upstream version 1.0.31
  * Fixed typo in long package description.
  * Updated Standards-Version: 4.5.0 (no changes needed)
  * Updated Erlang dependencies
  * Updated years in debian/copyright
  * Rules-Requires-Root: no

 -- Philipp Huebner <debalance@debian.org>  Wed, 05 Feb 2020 19:41:17 +0100

erlang-p1-sip (1.0.30-1) unstable; urgency=medium

  * New upstream version 1.0.30
  * Updated Erlang dependencies

 -- Philipp Huebner <debalance@debian.org>  Sat, 10 Aug 2019 14:25:26 +0200

erlang-p1-sip (1.0.29-1) unstable; urgency=medium

  * New upstream version 1.0.29
  * Updated Erlang dependencies
  * Updated Standards-Version: 4.4.0 (no changes needed)
  * debian/rules: export ERL_COMPILER_OPTIONS=deterministic
  * Updated debhelper compat version: 12

 -- Philipp Huebner <debalance@debian.org>  Wed, 24 Jul 2019 12:54:04 +0200

erlang-p1-sip (1.0.27-1) unstable; urgency=medium

  * New upstream version 1.0.27
  * Updated Erlang dependencies
  * Updated Standards-Version: 4.3.0 (no changes needed)
  * Updated years in debian/copyright

 -- Philipp Huebner <debalance@debian.org>  Tue, 01 Jan 2019 22:51:13 +0100

erlang-p1-sip (1.0.26-1) unstable; urgency=medium

  * New upstream version 1.0.26
  * Enabled DH_VERBOSE in debian/rules
  * Updated Standards-Version: 4.2.1 (no changes needed)

 -- Philipp Huebner <debalance@debian.org>  Sun, 07 Oct 2018 14:57:42 +0200

erlang-p1-sip (1.0.24-1) unstable; urgency=medium

  * New upstream version 1.0.24
  * Added debian/upstream/metadata

 -- Philipp Huebner <debalance@debian.org>  Wed, 04 Jul 2018 16:24:15 +0200

erlang-p1-sip (1.0.23-1) unstable; urgency=medium

  * New upstream version 1.0.23
  * Updated Standards-Version: 4.1.4 (no changes needed)

 -- Philipp Huebner <debalance@debian.org>  Wed, 09 May 2018 17:28:48 +0200

erlang-p1-sip (1.0.22-1) unstable; urgency=medium

  * New upstream version 1.0.22
  * Use secure copyright format uri in debian/copyright
  * Switched to debhelper 11

 -- Philipp Huebner <debalance@debian.org>  Tue, 27 Mar 2018 23:40:10 +0200

erlang-p1-sip (1.0.21-1) unstable; urgency=medium

  * New upstream version 1.0.21
  * (Build-)Depend on erlang-p1-tls (>= 1.0.20) and erlang-p1-stun (>= 1.0.20)

 -- Philipp Huebner <debalance@debian.org>  Sat, 13 Jan 2018 12:50:41 +0100

erlang-p1-sip (1.0.18-1) unstable; urgency=medium

  * Set Maintainer: Ejabberd Packaging Team <ejabberd@packages.debian.org>
  * Set Uploaders: Philipp Huebner <debalance@debian.org>
  * Updated Vcs-* fields in debian/control for salsa.debian.org
  * Updated years in debian/copyright
  * Updated Standards-Version: 4.1.3 (no changes needed)
  * New upstream version 1.0.18
  * (Build-)Depend on erlang-p1-tls (>= 1.0.18) and erlang-p1-stun (>= 1.0.17)

 -- Philipp Huebner <debalance@debian.org>  Thu, 04 Jan 2018 11:37:55 +0100

erlang-p1-sip (1.0.17-1) unstable; urgency=medium

  * New upstream version 1.0.17
  * (Build-)Depend on erlang-base (>= 1:19.2)
  * Updated Standards-Version: 4.1.1 (no changes needed)

 -- Philipp Huebner <debalance@debian.org>  Mon, 27 Nov 2017 20:25:51 +0100

erlang-p1-sip (1.0.15-1) unstable; urgency=medium

  * New upstream version 1.0.15
  * Updated Standards-Version: 4.1.0 (no changes needed)

 -- Philipp Huebner <debalance@debian.org>  Sat, 02 Sep 2017 17:40:08 +0200

erlang-p1-sip (1.0.13-1) unstable; urgency=medium

  * New upstream version 1.0.13
  * Updated Standards-Version: 4.0.0 (no changes needed)

 -- Philipp Huebner <debalance@debian.org>  Tue, 11 Jul 2017 18:48:02 +0200

erlang-p1-sip (1.0.11-1~exp1) experimental; urgency=medium

  * New upstream version 1.0.11
  * Updated debian/copyright

 -- Philipp Huebner <debalance@debian.org>  Sat, 15 Apr 2017 13:36:58 +0200

erlang-p1-sip (1.0.8-2) unstable; urgency=medium

  * Added erlang-base to Build-Depends

 -- Philipp Huebner <debalance@debian.org>  Wed, 05 Oct 2016 15:57:48 +0200

erlang-p1-sip (1.0.8-1) unstable; urgency=medium

  * New upstream version 1.0.8
  * (Build-)Depend on erlang-p1-utils (>= 1.0.5)
  * (Build-)Depend on erlang-p1-tls (>= 1.0.7)
  * (Build-)Depend on erlang-p1-stun (>= 1.0.7)

 -- Philipp Huebner <debalance@debian.org>  Fri, 16 Sep 2016 15:09:44 +0200

erlang-p1-sip (1.0.7-1) unstable; urgency=medium

  * Imported Upstream version 1.0.7
  * (Build-)Depend on erlang-p1-tls (>= 1.0.6)
  * (Build-)Depend on erlang-p1-stun (>= 1.0.6)

 -- Philipp Huebner <debalance@debian.org>  Fri, 05 Aug 2016 21:29:36 +0200

erlang-p1-sip (1.0.6-1) unstable; urgency=medium

  * Imported Upstream version 1.0.6

 -- Philipp Huebner <debalance@debian.org>  Sun, 03 Jul 2016 16:42:55 +0200

erlang-p1-sip (1.0.5-1) unstable; urgency=medium

  * Imported Upstream version 1.0.5
  * (Build-)Depend on erlang-p1-tls (>= 1.0.4)
  * (Build-)Depend on erlang-p1-stun (>= 1.0.4)

 -- Philipp Huebner <debalance@debian.org>  Sun, 26 Jun 2016 16:22:18 +0200

erlang-p1-sip (1.0.4-1) unstable; urgency=medium

  * Imported Upstream version 1.0.4
  * Increased erlang-p1-* (Build-)Depends to 1.0.3
  * Improved debian/watch
  * Updated Standards-Version: 3.9.8 (no changes needed)
  * Enabled hardening

 -- Philipp Huebner <debalance@debian.org>  Mon, 30 May 2016 20:07:03 +0200

erlang-p1-sip (1.0.2-1) unstable; urgency=medium

  * ProcessOne renamed p1_sip to esip
  * Imported Upstream version 1.0.2

 -- Philipp Huebner <debalance@debian.org>  Sun, 14 Feb 2016 18:38:42 +0100

erlang-p1-sip (1.0.1-1) unstable; urgency=medium

  * Imported Upstream version 1.0.1
  * Updated Standards-Version: 3.9.7 (no changes needed)
  * Updated Vcs-* fields in debian/control

 -- Philipp Huebner <debalance@debian.org>  Thu, 04 Feb 2016 15:36:15 +0100

erlang-p1-sip (1.0.0-1) unstable; urgency=medium

  * Imported Upstream version 1.0.0
  * Updated debian/copyright
  * Updated debian/watch

 -- Philipp Huebner <debalance@debian.org>  Sat, 16 Jan 2016 13:08:08 +0100

erlang-p1-sip (0.2015.07.22-1) unstable; urgency=medium

  * Imported Upstream version 0.2015.07.22
  * Enabled eunit

 -- Philipp Huebner <debalance@debian.org>  Mon, 17 Aug 2015 17:11:16 +0200

erlang-p1-sip (0.2015.04.17-2) unstable; urgency=high

  [ Holger Weiss ]
  * Support Erlang/OTP 18.x (Closes: #791371)

 -- Philipp Huebner <debalance@debian.org>  Sat, 04 Jul 2015 14:24:31 +0200

erlang-p1-sip (0.2015.04.17-1) unstable; urgency=medium

  * Imported Upstream version 0.2015.04.17
  * Updated Standards-Version: 3.9.6 (no changes needed)
  * Updated years in debian/copyright
  * Added Vcs links to debian/control
  * Streamlined debian/rules

 -- Philipp Huebner <debalance@debian.org>  Sat, 02 May 2015 18:14:07 +0200

erlang-p1-sip (0.2014.07.17-2) unstable; urgency=high

  * Fixed a FTBFS issue during dh_auto_install
  * Made debian/copyright lintian-clean

 -- Philipp Huebner <debalance@debian.org>  Tue, 09 Sep 2014 18:51:55 +0200

erlang-p1-sip (0.2014.07.17-1) unstable; urgency=medium

  * Imported Upstream version 0.2014.07.17

 -- Philipp Huebner <debalance@debian.org>  Thu, 28 Aug 2014 12:04:24 +0200

erlang-p1-sip (0.2014.06.12-1) unstable; urgency=medium

  * Initial release, see #744885 for background

 -- Philipp Huebner <debalance@debian.org>  Mon, 07 Jul 2014 22:12:13 +0200
